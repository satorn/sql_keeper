#!/bin/bash
echo start on $(date +%d/%m/%Y-%H:%M)

ZBX_CONN="/usr/bin/mysql"
ZBX_DATA="zabbix"
ZBX_USER="zabbix"
ZBX_PASS="ZBX#bay1234"
ZBX_HOST="127.0.0.1"

YEAR=$(expr $(date +%s) - 31536000)
MONTH=$(expr $(date +%s) - 2678400)
THREEMONTH=$(expr $(date +%s) - 8035200)

MONTH_TABLES="history history_uint history_str history_text history_log"
for table in $MONTH_TABLES;do
        DELETE=$( $ZBX_CONN -u $ZBX_USER -p$ZBX_PASS -D $ZBX_DATA -e "delete from $table where clock < '$MONTH' ;" )
        #echo " $DELETE from table $table"
	echo "  $(date +\%H:%M) $DELETE Delete table $table has been done"

done


YEAR_TABLES="alerts trends trends_uint"
for table in $YEAR_TABLES;do
        DELETE=$( $ZBX_CONN -u $ZBX_USER -p$ZBX_PASS -D $ZBX_DATA -e "delete from $table where clock < '$YEAR' ;" )
#        echo " $DELETE from table $table"
	echo "  $(date +\%H:%M) $DELETE Delete table $table has been done"
done

echo Finished on $(date +%d/%m/%Y-%H:%M)